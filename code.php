<?php


// Objects as Variables
$buildingObj = (object) [
	'name' => 'Caswyn Building',
	'floors' => 8,
	'address' => (object) [
		'barangay' => 'Sacred Heart',
		'city' => 'Quezon City',
		'country' => 'Philipphines'
	]
];


// Object from Classes

class Building {
	// properties
	private $name;
	protected $floors;
	protected $address;

	// Constructor

	public function __construct($name, $floors, $address) {
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	} 

	public function printName() {
		return "The name of the building is $this->name.";
	}

	public function printFloor() {
		return "The $this->name has $this->floors floors.";
	}

	public function printAddress() {
		return "The $this->name is located at $this->address.";
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}
};




// Instances
$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');





// Inherirance and Polymorphism

class Condominium extends Building {
	public function printName() {
		return "The name of the condominium is $this->name";
	}

	public function printFloor() {
		return "The $this->name has $this->floors floors.";
	}

	public function printAddress() {
		return "The $this->name is located at $this->address.";
	}

	// getters (read only) & setters (write-only)
	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}
};

// Instance
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');



// S3 Activity

class Person {

	// properties
	public $firstName;
	public $middleName;
	public $lastName;

	// constructor
	public function __construct($firstName, $middleName, $lastName) {
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName() {
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}
}

$person = new Person('Senku', 'Nomura', 'Ishigami');

class Developer extends Person {
	public function printName() {
		return "Your name is $this->firstName $this->lastName $this->lastName and you are a developer.";
	}
};

$developer = new Developer('John', 'Finch', 'Smith');


class Engineer extends Person {
	public function printName() {
		return "You are an engineer name $this->firstName $this->middleName $this->lastName";
	}
};

$engineer = new Engineer('Harold', 'Myers', 'Reese');









// End of the script
?>
